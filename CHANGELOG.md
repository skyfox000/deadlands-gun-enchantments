
# Changelog

Important changes.

## [2.1.0]

### Added
- Compatibility for foundry V11 (works in V10 too)
- Added Punch animation and sound for melee combat
### Changed
- NONE
### Removed
- NONE


## [2.0.4]

### Added
- Merge all the big changes from Nefarrio#1770 version that´s why there is a gap in the published versions from 1.8.2 to this one
### Changed
- Change some default images and sounds so there is no copyright problems.
### Removed
- NONE

## [2.0.3]

### Added
- NONE
### Changed (Thanks to Nefarrio#1770)
- Fixed issue where the shot effects caused an exception for extras when using swade-core or swade-tools.
### Removed
- NONE

## [2.0.2]

### Added
- NONE
### Changed (Thanks to Nefarrio#1770)
- Fixed issue where a single shot from one token results in multiple tokens firing on the same target.
### Removed
- NONE

## [2.0.1]

### Added (Thanks to Nefarrio#1770)
- Added a comma check and warning if any weapon or creature name string contains a comma.  This check is enabled/disabled with a configurable module setting.
### Changed (Thanks to Nefarrio#1770)
- Refactored how blood type is resolved and looked up.  There is now a dictionary for the various blood types.  This also resolved a bug introduced in 2.0.0 where the blood lookup was not working properly, causing the script to raise an exception (no effects at all end up being played).
### Removed
- NONE

## [2.0.0]

### Added (Really big thanks to Nefarrio#1770)
- Added a weapon data class (scripts/weapons.js) and dictionary that allows attributes to be assigned by weapon type.  The attributes are used to determine audio and visual effect behavior.  The dictionary entry for the weapon in use is now passed to the functions in scripts/shooting.js.
- Added a number of boolean attributes to the weapon class that define the characteristics of each weapon.  When thrown:  rotates, directional (doesn't rotate).  On a miss: ricochets, clicks, twangs.  On a hit:  makes a thud.  Other: has flame, explodes, has bullets, is an energy weapon.
- Added an data class for animation effects that is used within the new weapon data class.
- Added support for other thrown weapons:  knives, spears, hatchets, stones, and hammers.
- Added support for other fumble failure sounds:  twang for bows, short circuit for energy weapons.
- Added a couple extra sound effects for a hit:  a grunt for a regular hit, and a "Wilhelm" scream for a raise (configurable).
- Show setting hints on the configuration menus when hovering over the name of the setting.
### Changed (Again really big thanks to Nefarrio#1770)
- Refactored how shots are processed.  Previously, a single, overall status was determined for all rolls.  However, in SWADE, the result actually applies to each single die roll.  As a result, complex logic to determine how many shot effects to fire is no longer needed.
- Moved all weapon data to a dictionary that is only initialized at start up, or when weapon configuration changes.  Previously, it was building this dictionary each time a shot occurred.  This makes the module more efficient, cpu-wise.
- Moved/added a lot of things to const.js to make it easier for future changes.  Things can be changed in one place instead of many.
- Refactored the language files so that hints could be constructed in a phrase-like manner, resulting in the removal of many json entries.
- Changed weapon delimiter from a comma to a vertical bar (|), because some of the official SWADE items use commas in their names.
- Changed weapon search to use regular expressions instead of using exact names.  This means the user doesn't necessarily have to use the full weapon name in the config.
- Reduced the row size of the configuration menus so that it has a better fit on the screen.
- Tweaked some of the animation settings.
- Added a dynamically calculated delay so that the effects now play after (or pretty close) the dice results are shown in chat.
- With multiple shot attacks against multiple targets, the shots are rotated between each target.  Since there is no way yet to predict the future and tell how the player will want the shots divided, this performs fine.  Cinematically it shows shots going to many places.
- Added delay logic for the animation sequence in the case when using SWADE core module to give time for the rolls a chance to update in the chat window.
## [1.8.3]

### Added
- NONE
### Changed
- Fix some startup bugs (thanks Nefarrio#1770)
### Removed
- NONE

## [1.8.2]

### Added
- Add the foundry V9 support in the module.json
### Changed
- Fix for the Sequencer module 2.x
### Removed
- NONE

## [1.8.1]

### Added
- NONE
### Changed
- Fix a weird behaviour with the drag and drop of the names in blood types and gun types in the configuration that breaks some parts or the Swade system
### Removed
- NONE

## [1.8.0]

### Added
- Grenades type weapon added
### Changed
- Fix Problem when the gun and creatures names have singe quote (')
- Fix problem with settings drag and drop in some cases
- Fix some missing texts in settings
### Removed
- NONE

## [1.7.2]

### Added
- NONE
### Changed
- Fix Problem with 1.7.1 with missile guns
### Removed
- NONE

## [1.7.1]

### Added
- NONE
### Changed
- Fix sequencer warning
### Removed
- NONE

## [1.7.0]

### Added
- Added support for missile guns (bazooka, RPG, etc.)
### Changed
- Fix Swade-tools support (it was broken with the last Swade-tools update)
- Change all the configuration menu, make it mote friendly, now with drag and drop support for the guns (for the gun type config) and actors (for the blood config)
### Removed
- NONE

## [1.6.1]

### Added
- Added support for NPCs that were not WildCards
### Changed
- Fix bugs with Swade-tools when the token was linked with the actor data
### Removed
- NONE

## [1.6.0]

### Added
- Added support for blood of 3 other colors, black, blue and green, you have to configure which creatures have which blood
### Changed
- Fix bugs with Swade-tools and the base system in some cases
### Removed
- NONE

## [1.5.0]

### Added
- Added single bullet shot animation with default file from JB2A - Jules&Ben's Animated Assets module
- Added multi shot flash animation with default file from Jinker's Animated Art Pack module
- Added code for the missing shot in the single shot, multi shot, single laser, multi laser and arrows
- Added a "beta test" for a weapon "throw" animation, if you roll an attack with a weapon called "Tomahawk" it should play the animation
### Changed
- Fix bugs in guns with ROF > 1
- Change the default animation for the simple laser shot with a B2A - Jules&Ben's Animated Assets module animation
- Change the sound for the single laser shot to match the new animation
### Removed
- NONE

## [1.4.0]

### Added
- Dependency with Sequencer module (make my work easier)
- Added the blood animations on success and raise rolls
- Added the missed shot on fail rolls
- Added the burn animation for the flamethrower
### Changed
- Refactor almost all the shooting code to change the use of FxMaster module with the Sequencer module, reducing the amount of code considerably
- fix bugs in guns with ROF > 1
- fix typo in the route for the arrow animation for the bow
- fix some bug when using swade-tools module
### Removed
- Dependency with FX Master module (Replaced with dependency with the Sequencer module)

## [1.3.1]

### Added
- NONE
### Changed
- Update support for fxMaster 1.2.0 (some changes in the canvas management)
- fix bug that happens with new versions of fxMaster (from the 1.1.4)
### Removed
- NONE

## [1.3.0]

### Added
- Support for Foundry VTT 0.8.X, that basically includes changes in the settings management, (I had not uploaded the changes before because I was waiting for the better rolls module to be updated and for the base system to correct a bug that affected me, unfortunately the swade-tools module has not been updated to foundry 0.8 yet so it does not work, but if they update my module should work with them without problems)
### Changed
- NONE
### Removed
- Remove some code that I duplicate and change in my module from the FxMaster module (they make some suggested changes by me in one of their parameters, so I can now use their module directly)

## [1.2.2]

### Added
- NONE
### Changed
- Fix a bug that was breaking the module if the betterRolls module was not installed
- Changed the controls in the settings for the animation files to FilePicker instead of text Box
- fix some problems in the language files
### Removed
- NONE

## [1.2.1]

### Added
- NONE
### Changed
- Fix the propagation of the animations to the other players (it was not working in 1.2.0).
- Fix a little bug when the player close the skill roll windows without actually doing the roll.
### Removed
- NONE

## [1.2.0]

### Added
- Added new animations and sounds for more modern gun types (machine guns, automatic guns, lasers, etc.)
- Added fire animation and sound for the  flamethrower type guns  :smiling_imp:
### Changed
- Fix bug with the betterRolls autoroll feature (thank you Javier).
- Now some animations are adjusted in lenght taking into account the distance to the target.
- Make some minor modifications to better organizing the code
- Change the order of the versions in this list (newest on top)
### Removed
- Removed the dependency with the "[Jack Kerouac's Animated Spell Effects](https://foundryvtt.com/packages/animated-spell-effects)" module, now the few animations that i´m using are inside the module (having 1 gigabyte dependency for only 2.6 megabytes of real dependency is not logical).
- Remove the skill name (Shooting) check, now if the name of the item is the same as the configuration, the animation and the sound associated will play without taking into account the skill used, it was an unnecessary validation, a secundary consequence of this change is that the module should work now with all languages.

## [1.1.0]

### Added
- Suport for "[SWADE Tools](https://foundryvtt.com/packages/swade-tools)" module.
### Changed
- Fix bug when firing weapons with RoF > 1 in SWADE core system.
### Removed
- NONE


## [1.0.0]

###

- Initial Release  
