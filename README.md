
# Deadlands (SWADE) Gun Enchantments

Make your weapons feel more real in your Deadlands (SWADE) games in foundry VTT, with different sounds and animation for each "type" of gun, the "[Sequencer](https://foundryvtt.com/packages/sequencer)" module is required to play the animations and sounds, all the animations and sounds are included in the module, the default animation files are from the "[Jack Kerouac's Animated Spell Effects](https://foundryvtt.com/packages/animated-spell-effects)" and the "[JB2A - Jules&Ben's Animated Assets](https://foundryvtt.com/packages/JB2A_DnD5e)"  modules, but you can configure your own ones if you want!!.

Now with **Blood**, **Fire** and **Missed shots** animations!!!, thanks to the great animations from "[Jinker's Animated Art Pack](https://foundryvtt.com/packages/jaamod)"

## How It Works

The module captures the moment in which **the selected token** makes an item skill roll, looks for the name of that item, compares it with the configuration to know if the items belong to a specific type of weapon, obtains the roll´s result and plays the animations and a sounds depending on the case:

- If the roll was a **success** (with or without a "*raise*") the sound of the weapon is played (different depending on the weapon type) as well as the animation of the weapon's flash (for some types) and the shot bullet(s) animation, and then plays the blood splash animation in the target(s), if the result was a raise the blood splash is a little bigger.
- If the roll was a **failure**, apart from the sound that is played in the case of **success**, a "*ricochet*" sound of the bullet is also reproduced (does not apply in the case of arrows or some other types that don't ricochet), the bullet animation will miss the target, and also a bullet impact in the ground missind the target animation will play.
- If the roll is a **fumble** or critical failure, neither animation nor the sound of the shot is produced, but instead a failure sound ("*click*", "*twang*", or "*short-circuit*") is played to represent that the weapon is jammed or some other problem occurred.

## Configuration

  This module requires the following data to be configured:

1. **Various weapon animations:** This parameter indicates the path of a file with an animation of the weapon effect.  There are options for muzzle flash as well as projectile path.  Animation settings include:  single shot, multiple shots, burst shot, projectile weapons, energy weapons, flame-throwing weapons, explosions, and thrown weapons.\
\
The following Foundry VTT modules provide some of the default effect selections:\
"[Jack Kerouac's Animated Spell Effects](https://foundryvtt.com/packages/animated-spell-effects)"\
"[JB2A - Jules&Ben's Animated Assets](https://foundryvtt.com/packages/JB2A_DnD5e)"\
"[Jinker's Animated Art Pack](https://foundryvtt.com/packages/jaamod)"

2. **List of each weapon type:** you have to provide a list of weapon names for the specific type, separated with a vertical bar (|).  The weapon types are:  Derringers, Single Action Revolvers, Double Action Revolvers, Carbines, Rifles, Shotguns, Gatling Guns, Automatics, Assault Weapons, Laser Pistols, Laser Machine Guns, Flamethrowers, Bows, Rockets, Grenades, Dynamite, Hatchets, Knives, Tomahawks, Spears, Hammers, and Stones.

3. **List of creatures with different colored blood:** you have to provide a list of creature names (*exactly the same name of the creature*) that you want to have for each color blood, separated with a vertical bar (|).  The blood types are:  black, blue, and green.

4. **Enable "Wilhelm" scream effect:** on a raise, this setting will result in the playing of the "Wilhelm" scream audio effect.  For the curious, this sound effect has been around since 1951, and can be heard in dozens and dozens of movies, television shows, and video games over many years, and is still in use today.  Notable uses:  a number of the Star Wars, Indiana Jones, and Disney movies.  (https://en.wikipedia.org/wiki/Wilhelm_scream)

5. **Enable comma delimiter check:** since the module now uses a vertical bar (|) to delimiting name lists instead of commas, this setting enables the module to check for commas in weapon and creature settings.  If a comma is present, a message will be displayed indicating that the lists should be checked, and converted to use vertical bars if necessary.  When disabled, no check or message will appear.

## Compatibility

This module is created to work under the [SWADE](https://foundryvtt.com/packages/swade/) system and is compatible with the "[Better Rolls 2 for Savage Worlds](https://foundryvtt.com/packages/betterrolls-swade2)" module, and with "[SWADE Tools](https://foundryvtt.com/packages/swade-tools)" module, I have tested it with other SWADE modules and I have not found any incompatibilities yet.

## Known limitations or problems

-  If you are using the [Dice So Nice!](https://foundryvtt.com/packages/dice-so-nice/) Module with the core [SWADE](https://foundryvtt.com/packages/swade/) system or with the [SWADE Tools](https://foundryvtt.com/packages/swade-tools) module, the animation and the sound are delayed a little to allow the dice to update in the chat window.  It works without delay in the case of the "[Better Rolls 2 for Savage Worlds](https://foundryvtt.com/packages/betterrolls-swade2)" module.
-   Due to the way the animations adapt to the distance between the tokens with the module that I am using to play the animations, in cases where the tokens are too far or too close, the animation can be seen too large or too small, I am trying to see how you can adapt it better but i think theres nothing I can do, in some extreme cases is not going to look ok no matter what I do.
-   If there are many animations playing at the same time (example: firing a gun with ROF 3, to 3 targets with some hits and some misses), it is possible that some animations will play with delay or not displayed at all, but this tends to happen with computers with a weak video card, and happens more in Chrome than in Firefox.

## Future TO-DO
- *Short and/or Medium and/or long Term:* I don´t have anything in mind right now... open to ideas... maybe more gun types?... general sounds when hit or raise?

## Contribution
- If you'd like to support my work, feel free to leave a tip through [Paypal](https://www.paypal.com/donate?business=skyfox000@gmail.com&currency_code=EUR)
- If you want to contribute in the code you can create a branch and make a merge request.
- If you have any idea or request, open a requirement and I will try to review it (with the limitations that time allows me)
- If you want to contact me I´m in Discord (randyrosales#8073)
- If you have created gun/weapons animations and/or sounds and want them to be included in the module, you are welcome to do it, please tell me and I´ll be happy to include them.

## License

- The module source code is licensed under [GPL-3.0](https://www.gnu.org/licenses/gpl-3.0.html).
- The module [Sequencer](https://foundryvtt.com/packages/sequencer) that i use to play the animations and sounds is under the [MIT license](https://opensource.org/licenses/MIT)
- The sounds come from two sources:
  * From [soundbible.com](https://soundbible.com/) using de [Attribution 3.0](https://creativecommons.org/licenses/by/3.0/) license and/or the [Sampling Plus 1.0](https://creativecommons.org/licenses/sampling+/1.0/) license
  * From [zapsplat](https://www.zapsplat.com) using the "Standard License" of that site
- The animations included by default are from this great modules:
  * [Jack Kerouac's Animated Spell Effects](https://foundryvtt.com/packages/animated-spell-effects) that are licensed under [GPL-3.0](https://www.gnu.org/licenses/gpl-3.0.html).
  *  [JB2A - Jules&Ben's Animated Assets](https://foundryvtt.com/packages/JB2A_DnD5e) that are licensed under [Creative Commons License Attribution-NonCommercial CC BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/) ---  [Patreon](https://www.patreon.com/JB2A)
  * [Jinker's Animated Art Pack](https://foundryvtt.com/packages/jaamod)  (free assets module) --- [Patreon](https://www.patreon.com/jinker)
- Some images are from [OpenClipart-Vectors](https://pixabay.com/users/openclipart-vectors-30363/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1300043), [W00D00W](https://pixabay.com/users/w00d00w-16996861/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=5295036) and [Clker-Free-Vector-Images](https://pixabay.com/users/clker-free-vector-images-3736/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=297185) in [Pixabay](https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1300043)
