import {
  moduleName,
  PATH,
  RESULT,
  debug,
} from './const.js';
import {
  getBlood,
} from './blood.js';
import {
  getWeapon,
} from './weapons.js';
import {
  DEADLANDS_BLOOD,
} from './init.js';

// Fire the weapon, with animation and sound
export function fireWeapon(sequence, shooter, target, weaponName, result) {
  debug('fireWeapon: shooter ' + shooter.name + ' target ' + target.name +
        ' weaponName ' + weaponName + ' result ' + result);
  if (result == RESULT.IGNORE) {
    return;
  }

  // The the parameters for the weapon name
  var weapon = getWeapon(weaponName);
  // there are parameters for this weapon?
  if (weapon != null) {
    debug('weapon', weapon);
    // Set the animation and sound for the sequence
    setWeaponEffects(sequence, shooter, target, weapon, result);
    // Set the blood and sounds for the sequence
    setResultEffects(sequence, shooter, target, weapon, result);
  } else {
    debug('Unable to find setting for ' + weaponName);
  }
}

// Set the weapon animation and sound to play
function setWeaponEffects(sequence, shooter, target, weapon, result) {
  debug('setWeaponEffects: shooter ' + shooter.name + ' target ' + target.name + ' result ' + result);
  switch (result) {
    case RESULT.RAISE:
    case RESULT.SUCCESS:
    case RESULT.FAIL:
      //Set the shot flash parameters
      setFlash(sequence, shooter, target, weapon);
      //set the shot parameters
      setShot(sequence, shooter, target, weapon, result);
      //play shooting sound
      debug('Adding weapon sound...');
      sequence.wait(weapon.delayAudio);
      sequence.sound()
        .file(weapon.audio)
        .volume(weapon.volume);
      break;

    case RESULT.FUMBLE:
      // Mechanical misfire.
      if (weapon.hasClick) {
        debug('Adding click sound...');
        sequence.sound()
          .file(PATH.AUDIO + 'click.ogg');
      }

      // Bow misfire.
      if (weapon.hasTwang) {
        debug('Adding twang sound...');
        sequence.sound()
          .file(PATH.AUDIO + 'twang.ogg');
      }

      // Energy misfire.
      if (weapon.hasEnergy) {
        debug('Adding bzzzt sound...');
        sequence.sound()
          .file(PATH.AUDIO + 'short_circuit.ogg');
      }

      //Punch Misses
      if (weapon.isMelee) {
        debug('Adding whoosh sound...');
        sequence.sound()
          .file(PATH.AUDIO + 'whoosh.ogg');
      }


      break;

    default: //??
      break;
  }
}

// Set the shot flash configuration with sequencer
function setFlash(sequence, shooter, target, weapon) {
  debug('setFlash: weapon ' + weapon.name);
  if (weapon.flash.file) {
    let seq = sequence.effect()
      .file(weapon.flash.file)
      .atLocation(shooter)
      .rotateTowards(target);

    if (weapon.flash.scaleX === "auto") {
      seq.stretchTo(target);
    } else {
      seq.scale({
          x: weapon.flash.scaleX,
          y: weapon.flash.scaleY
        })
        .anchor({
          x: weapon.flash.x,
          y: weapon.flash.y
        });
    }
  }
}

// Set the shot configuration with sequencer
function setShot(sequence, shooter, target, weapon, result) {
  debug('setShot: weapon ' + weapon.name);
  if (weapon.animation.file) {
    let seq = sequence.effect()
      .file(weapon.animation.file)
      .atLocation(shooter);

    if (weapon.animation.scaleX === "auto") {
      if (!weapon.hasRotation && !weapon.hasDirection) {
        seq.stretchTo(target);
      }

      // Adjustment for DND5E animations.
      let regex = new RegExp( '(JB2A_DnD5e|(05|15|30|60|90|120)ft)' );
      if (weapon.animation.file.search( regex ) != -1) {
        seq.template({ gridSize: 200, startPoint: 200, endPoint: 200 });
      }

      if (result == RESULT.FAIL) {
        // Miss, so pick location not quite on target.
        seq.missed();
      }
    } else {
      seq.rotateTowards(target);
      seq.scale({
          x: weapon.animation.scaleX,
          y: weapon.animation.scaleY
        })
        .anchor({
          x: weapon.animation.x,
          y: weapon.animation.y
        });
    }

    // Rotating ammunition
    if (weapon.hasRotation) {
      seq.rotateIn(1440, 1000)
        .moveTowards(target);
    } else {
      if (weapon.hasDirection) {
        seq.moveTowards(target);
      }
    }
    seq.wait(10);
  }
}


// The blood and miss animation management... complicated with ROF > 1 and targets > 1 but works.... i think
function setResultEffects(sequence, shooter, target, weapon, result) {
  debug('setResultEffects: shooter ' + shooter.name + ' target ' + target.name + ' result ' + result);

  if (result == RESULT.FUMBLE) {
    return;
  }

  if ((result == RESULT.SUCCESS) || (result == RESULT.RAISE)) {
    // wait to syncronize effects
    sequence.wait(weapon.delayResult);

    if (weapon.hasExplosion) {
      debug('Adding explosion effect and sound...');
      let seq = sequence.effect()
        .file(PATH.VIDEO + 'boom4.webm')
        .scale(1)
        .anchor(0.5)
        .atLocation(target, {randomOffset: 0.75})
        .randomizeMirrorX()
        .randomizeMirrorY();
      sequence.sound()
        .file(PATH.AUDIO + 'explosion.ogg');
    }

    let seq = sequence.effect();
    let dir = PATH.VIDEO;
    let file;

    if (weapon.hasFire) {
      debug('Adding fire effect...');
      seq.anchor(0.5);
      seq.wait(500);
      file = dir + 'fireFlat.webm';
    } else {
      let blood = getBlood(target.name);
      debug('Adding ' + blood?.name + ' blood effect...');
      file = blood.file;
      //seq.repeats(bulletPerTarget[total - 1], 0.01, 0.05)
      seq.playbackRate(15);
    }

    // common parameters
    seq.file(file)
      .atLocation(target, {randomOffset: 0.75})
      .randomizeMirrorX()
      .randomizeMirrorY();

    // Adjust effect size based on level of success.
    if (result == RESULT.RAISE) {
      seq.scale( weapon.hasFire ? 0.20 : 0.45 );
    } else {
      seq.scale( weapon.hasFire ? 0.10 : 0.25);
    }

    if (weapon.hasThud) {
      debug('Adding thud sound...');
      sequence.sound()
        .file(PATH.AUDIO + 'thud.ogg');
    }

    // On a hit, play a sound for the target's reaction.
    let sound;
    if ((result == RESULT.RAISE) && game.settings.get(moduleName, 'scream')) {
      // Wilhelm scream on raise (if enabled).
      debug('Adding wilhelm scream...');
      sound = { audio : 'wilhelm.ogg', volume : 0.5, delay : 1 };
    } else {
      // Otherwise a grunt.
      debug('Adding grunt sound...');
      sound = { audio : 'grunt.ogg', volume : 1.0, delay : 500 };
    }
    sequence.wait(sound.delay);
    sequence.sound()
      .file(PATH.AUDIO + sound.audio)
      .volume(sound.volume);
  } else {
    // Misses!
    let bow = false;
    if (weapon.hasRicochet) {
      debug('Adding ricochet sound...');
      sequence.sound()
        .file(PATH.AUDIO + 'ricochet.ogg')
        .volume(0.2);
    }

    let seq;
    if (weapon.hasBullet || weapon.hasEnergy ) {
      debug('Adding explosion effect (small)...');
      seq = sequence.effect()
        .file(PATH.VIDEO + 'boom4.webm')
        //.repeats(bulletPerTarget[total - 1], 0.01, 0.05)
        .scale(0.05)
        .playbackRate(15);
    } else {
      if (weapon.hasFire) {
        debug('Adding fire effect (small)...');
        seq = sequence.effect()
          .file(PATH.VIDEO + 'fireFlat.webm')
          .scale(0.25)
          .anchor(0.5);
      } else {
        if (weapon.hasExplosion) {
          debug('Adding explosion effect and sound...');
          sequence.wait(weapon.delayResult);
          seq = sequence.effect()
            .file(PATH.VIDEO + 'boom4.webm')
            .scale(1);
          sequence.sound()
            .file(PATH.AUDIO + 'explosion.ogg');
        } else {
          bow = true;
        }
      }
    }

    if (!bow) {
      // common parameters for fumble and fails
      seq.atLocation(target)
        .randomizeMirrorX()
        .randomizeMirrorY()
        // Miss, so pick location not quite on target.
        .missed();
    }
  }
}
