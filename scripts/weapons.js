import {
  moduleName,
  PATH,
  FLAG,
  WEAPON,
  ANIMATION,
  debug,
} from './const.js';
import {
  DEADLANDS_WEAPON,
} from './init.js';
import {
  checkForCommas,
} from './configuration/settings.js';

class EffectData {
  constructor(setting, scaleX, scaleY, x, y) {
    debug('effect constructor: setting ' + setting);
    this.file = setting ? game.settings.get(moduleName, setting) : '';
    this.scaleX = scaleX;
    this.scaleY = scaleY;
    this.x = x;
    this.y = y;
  }
}

class Weapon {
  constructor(name, audio, volume, delay=null) {
    this.name = name;
    this.audio = audio ? (PATH.AUDIO + audio) : '';
    this.volume = volume;
    this.settingName = name;
    this.setting = game.settings.get(moduleName, name);
    // Trim the weapon list entries.
    this.list = this.setting.split('|').map(el => el.trim());
    // Build a regex for later use; escape certain special characters.
    let regexStr = this.list.join('|').replace(/[-[/\]{}()*+?.,\\^$#]/g, '\\$&');
    this.regex = new RegExp('.?(' + regexStr + ')');
    // Mimimum delay of 1.
    this.delayResult = delay?.result ? delay.result : 1;
    this.delayAudio = delay?.audio ? delay.audio : 1;
    // Behavioral flags.
    this.hasRotation = false;
    this.hasRicochet = false;
    this.hasClick = false;
    this.hasTwang = false;
    this.hasFire = false;
    this.hasExplosion = false;
    this.hasBullet = false;
    this.hasEnergy = false;
    this.hasThud = false;
    this.hasDirection = false;
    this.isMelee = false;
  }

  setQualifiers(flags) {
    this.hasRotation  = (flags & FLAG.ROTATES);
    this.hasRicochet  = (flags & FLAG.RICOCHET);
    this.hasClick     = (flags & FLAG.CLICK);
    this.hasTwang     = (flags & FLAG.TWANG);
    this.hasFire      = (flags & FLAG.FIRE);
    this.hasExplosion = (flags & FLAG.EXPLODES);
    this.hasBullet    = (flags & FLAG.BULLET);
    this.hasThud      = (flags & FLAG.THUDS);
    this.hasEnergy    = (flags & FLAG.ENERGY);
    this.hasDirection = (flags & FLAG.DIRECTION);
    this.isMelee      = (flags & FLAG.MELEE);
  }

  setAnimation(setting, scaleX, scaleY, x, y) {
    this.animation = new EffectData(setting, scaleX, scaleY, x, y);
  }

  setFlash(setting, scaleX, scaleY, x, y) {
    this.flash = new EffectData(setting, scaleX, scaleY, x, y);
  }
}

export function initWeapons() {
  let guns = DEADLANDS_WEAPON;

  // Projectile weapons
  let i = WEAPON.DERRINGER.setting;
  guns[i] = new Weapon(i, 'derringer.ogg', 0.3);
  guns[i].setFlash(    ANIMATION.FLASH.setting,          0.1, 0.1, -0.08, 0.5);
  guns[i].setAnimation(ANIMATION.SINGLE_SHOT.setting, 'auto', 1.0, -0.08, 0.5);
  guns[i].setQualifiers( FLAG.RICOCHET | FLAG.CLICK | FLAG.BULLET );

  i = WEAPON.SINGLE_ACTION.setting;
  guns[i] = new Weapon(i, 'revolver-single-action.ogg', 0.4);
  guns[i].setFlash(    ANIMATION.FLASH.setting,          0.3, 0.2, -0.08, 0.5);
  guns[i].setAnimation(ANIMATION.SINGLE_SHOT.setting, 'auto', 1.0, -0.08, 0.5);
  guns[i].setQualifiers(FLAG.RICOCHET | FLAG.CLICK | FLAG.BULLET );

  i = WEAPON.DOUBLE_ACTION.setting;
  guns[i] = new Weapon(i, 'revolver-double-action.ogg', 0.4);
  guns[i].setFlash(    ANIMATION.FLASH.setting,          0.3, 0.2, -0.08, 0.5);
  guns[i].setAnimation(ANIMATION.SINGLE_SHOT.setting, 'auto', 1.0, -0.08, 0.5);
  guns[i].setQualifiers( FLAG.RICOCHET | FLAG.CLICK | FLAG.BULLET );

  i = WEAPON.CARBINE.setting;
  guns[i] = new Weapon(i, 'carbine.ogg', 0.6);
  guns[i].setFlash(    ANIMATION.FLASH.setting,          0.4, 0.3, -0.08, 0.5);
  guns[i].setAnimation(ANIMATION.SINGLE_SHOT.setting, 'auto', 1.0, -0.08, 0.5);
  guns[i].setQualifiers( FLAG.RICOCHET | FLAG.CLICK | FLAG.BULLET );

  i = WEAPON.RIFLE.setting;
  guns[i] = new Weapon(i, 'rifle.ogg', 0.6);
  guns[i].setFlash(    ANIMATION.FLASH.setting,          0.4, 0.3, -0.08, 0.5);
  guns[i].setAnimation(ANIMATION.SINGLE_SHOT.setting, 'auto', 1.0, -0.08, 0.5);
  guns[i].setQualifiers( FLAG.RICOCHET | FLAG.CLICK | FLAG.BULLET );

  i = WEAPON.SHOTGUN.setting;
  guns[i] = new Weapon(i, 'shotgun.ogg', 0.6);
  guns[i].setFlash(    ANIMATION.FLASH.setting,      0.3, 0.4, -0.08, 0.5);
  guns[i].setAnimation(ANIMATION.SHOTGUN.setting, 'auto', 1.0, -0.08, 0.5);
  guns[i].setQualifiers( FLAG.RICOCHET | FLAG.CLICK | FLAG.BULLET );

  i = WEAPON.GATLING.setting;
  guns[i] = new Weapon(i, 'gatling.ogg', 1.0, { 'result' : 500 });
  guns[i].setFlash(    ANIMATION.BARRAGE_FLASH.setting,    0.1, 0.1,  0.1, 0.5);
  guns[i].setAnimation(ANIMATION.BARRAGE.setting,       'auto', 1.0, -0.1, 0.5);
  guns[i].setQualifiers( FLAG.RICOCHET | FLAG.CLICK | FLAG.BULLET );

  i = WEAPON.AUTOMATIC.setting;
  guns[i] = new Weapon(i, 'automatic.ogg', 0.8);
  guns[i].setFlash(    ANIMATION.FLASH.setting,          0.3, 0.2, -0.08, 0.5);
  guns[i].setAnimation(ANIMATION.SINGLE_SHOT.setting, 'auto', 0.5,     0, 0.5);
  guns[i].setQualifiers( FLAG.RICOCHET | FLAG.CLICK | FLAG.BULLET );

  i = WEAPON.ASSAULT.setting;
  guns[i] = new Weapon(i, 'assault.ogg', 1.0);
  guns[i].setFlash(    ANIMATION.BARRAGE_FLASH.setting,    0.1, 0.1,  0.1, 0.5);
  guns[i].setAnimation(ANIMATION.BARRAGE.setting,       'auto', 1.0, -0.1, 0.5);
  guns[i].setQualifiers( FLAG.RICOCHET | FLAG.CLICK | FLAG.BULLET );

  i = WEAPON.LASER.setting;
  guns[i] = new Weapon(i, 'laser.ogg', 1.0);
  guns[i].setFlash(    '',                          '',   0,     0,   0);
  guns[i].setAnimation(ANIMATION.LASER.setting, 'auto', 1.0, -0.08, 0.5);
  guns[i].setQualifiers( FLAG.ENERGY );

  i = WEAPON.LASER_MACHINE.setting;
  guns[i] = new Weapon(i, 'lasermachine.ogg', 0.6);
  guns[i].setFlash(    '',                                  '',   0, 0,   0);
  guns[i].setAnimation(ANIMATION.LASER_BARRAGE.setting, 'auto', 1.0, 0, 0.5);
  guns[i].setQualifiers( FLAG.ENERGY );

  i = WEAPON.FLAME.setting;
  guns[i] = new Weapon(i, 'flame.ogg', 1.0);
  guns[i].setFlash(    '',                       '',   0,     0,   0);
  guns[i].setAnimation(ANIMATION.FLAME.setting, 1.0, 1.0, -0.04, 0.5);
  guns[i].setQualifiers( FLAG.CLICK | FLAG.FIRE );

  i = WEAPON.BOW.setting;
  guns[i] = new Weapon(i, 'bow.ogg', 1.0, { 'result' : 250, 'audio' : 500 });
  guns[i].setFlash(    '',                        '',   0,     0,   0);
  guns[i].setAnimation(ANIMATION.BOW.setting, 'auto', 1.0, -0.08, 0.5);
  guns[i].setQualifiers( FLAG.THUDS | FLAG.TWANG );

  i = WEAPON.ROCKET.setting;
  guns[i] = new Weapon(i, 'rocket.ogg', 1.0, { 'result' : 3000 });
  guns[i].setFlash(    ANIMATION.FLASH.setting,  0.5, 0.4, 0.1, 0.5);
  guns[i].setAnimation(ANIMATION.ROCKET.setting, 0.1, 0.1,   0, 0.5);
  guns[i].setQualifiers( FLAG.CLICK | FLAG.EXPLODES | FLAG.DIRECTION );

  i = WEAPON.GRENADE.setting;
  guns[i] = new Weapon(i, 'throw.ogg', 1.0, { 'result' : 900 });
  guns[i].setFlash(    '',                          '',    0,     0,   0);
  guns[i].setAnimation(ANIMATION.GRENADE.setting, 0.05, 0.05, -0.08, 0.5);
  guns[i].setQualifiers( FLAG.ROTATES | FLAG.EXPLODES );

  i = WEAPON.DYNAMITE.setting;
  guns[i] = new Weapon(i, 'throw.ogg', 1.0, { 'result' : 900 });
  guns[i].setFlash(    '',                          '',   0,     0,   0);
  guns[i].setAnimation(ANIMATION.DYNAMITE.setting, 0.2, 0.2, -0.08, 0.5);
  guns[i].setQualifiers( FLAG.ROTATES | FLAG.EXPLODES );

  // Thrown weapons
  i = WEAPON.HATCHET.setting;
  guns[i] = new Weapon(i, 'whoosh.ogg', 1.0, { 'result' : 1000 });
  guns[i].setFlash(    '',                         '',   0,     0,   0);
  guns[i].setAnimation(ANIMATION.HATCHET.setting, 0.1, 0.1, -0.08, 0.5);
  guns[i].setQualifiers( FLAG.ROTATES | FLAG.THUDS );

  i = WEAPON.KNIFE.setting;
  guns[i] = new Weapon(i, 'whoosh.ogg', 1.0, { 'result' : 1000 });
  guns[i].setFlash(    '',                          '',   0,     0,   0);
  guns[i].setAnimation(ANIMATION.KNIFE.setting, 'auto', 0.5, -0.08, 0.5);
  guns[i].setQualifiers( FLAG.THUDS );

  i = WEAPON.TOMAHAWK.setting;
  guns[i] = new Weapon(i, 'whoosh.ogg', 1.0, { 'result' : 1000 });
  guns[i].setFlash(    '',                           '',    0,     0,   0);
  guns[i].setAnimation(ANIMATION.TOMAHAWK.setting, 0.15, 0.15, -0.08, 0.5);
  guns[i].setQualifiers( FLAG.ROTATES | FLAG.THUDS );

  i = WEAPON.SPEAR.setting;
  guns[i] = new Weapon(i, 'whoosh.ogg', 1.0, { 'result' : 1000 });
  guns[i].setFlash(    '',                       '',   0,     0,   0);
  guns[i].setAnimation(ANIMATION.SPEAR.setting, 0.2, 0.2, -0.08, 0.5);
  guns[i].setQualifiers( FLAG.THUDS | FLAG.DIRECTION );

  i = WEAPON.HAMMER.setting;
  guns[i] = new Weapon(i, 'whoosh.ogg', 1.0, { 'result' : 1000 });
  guns[i].setFlash(    '',                        '',   0,     0,   0);
  guns[i].setAnimation(ANIMATION.HAMMER.setting, 0.1, 0.1, -0.08, 0.5);
  guns[i].setQualifiers( FLAG.ROTATES | FLAG.THUDS );

  i = WEAPON.STONE.setting;
  guns[i] = new Weapon(i, 'whoosh.ogg', 1.0, { 'result' : 1000 });
  guns[i].setFlash(    '',                       '',   0,     0,   0);
  guns[i].setAnimation(ANIMATION.STONE.setting, 0.1, 0.1, -0.08, 0.5);
  guns[i].setQualifiers( FLAG.ROTATES | FLAG.THUDS );

  i = WEAPON.PUNCH.setting;
  guns[i] = new Weapon(i, 'punch.ogg', 1.0, { 'result' : 1000 });
  guns[i].setFlash(    '',                           '',    0,     0,   0);
  guns[i].setAnimation(ANIMATION.PUNCH.setting, 0.5, 0.5, 0.4, 0.5);
  guns[i].setQualifiers( FLAG.MELEE );

  // Check for comma delimiters.
  checkForCommas(guns);
}

// Find the weapon based on its name.
export function getWeapon(name) {
  let weapon = null;

  for (let key in DEADLANDS_WEAPON) {
    let checkWeapon = DEADLANDS_WEAPON[key];
    let pos = name.search(checkWeapon.regex);
    if (pos != -1) {
      weapon = checkWeapon;
      break;
    }
  };

  return weapon ? weapon : null;
}
