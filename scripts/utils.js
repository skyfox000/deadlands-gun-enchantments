import {
  RESULT,
  DELAY,
  MODULE,
  debug,
} from './const.js';
import {
  fireWeapon,
} from './shooting.js';

let timeoutId;
let timeoutSeq;

export async function checkRollsAndShoot(message, swadeItem, roll, module, actor) {
  // Get the controlled token and actor.
  const shooter = canvas.tokens.controlled[0];
  let me = shooter.actor;
  debug('checkRollsAndShoot: module ' + module + ' shooter ' + shooter?.name +
        ' me ' + me.name + ' item ' + swadeItem.name + ' roll ', roll);

  // no token selected
  if (!shooter) {
    //ui.notifications.error(game.i18n.localize("DeadlandsEnchantments.NoToken"));
    return;
  }

  // Only the shooter should generate the effect!
  if (actor != me) {
    debug(me.name + ' is not the shooter, nothing to do.');
    return;
  }

  if (roll) {
    if (module == MODULE.SWADETOOLS) {
      // Swade-tools might update just the flags, so ignore in this case.
      if (message.data.flags['swade-tools'].flagupdateonly) {
        debug('Bypassing swade-tools flag update message.');
        return;
      }
    }

    // These variables are used below, and are set based on module type.
    let rolls;
    let trait_roll;
    let dice = [];
    let isFumble;
    let isWildcard = shooter.actor.isWildcard;

    // Evaluate the results for each set of die rolls, based on module being used.
    // Assemble the final dice totals and target numbers in an array.
    if (module == MODULE.SWADECORE || module == MODULE.SWADETOOLS) {
      // Possibly deal with an extra modifiers.
      let modifier = getOuterModifier(roll.terms);
      rolls = roll.terms[0].results;
      isFumble = isFumbleRoll(rolls);
      // Assume target number of 4 in, because it is not provided, and is complex to calculate.
      rolls.forEach(roll => { dice.push({ value : (roll.result + modifier), tn : 4 }) });
    } else {
      // Extra modifiers are already applied with BetterRolls.
      let render_data = message.getFlag('betterrolls-swade2', 'render_data');
      trait_roll = render_data.trait_roll;
      isFumble = trait_roll.is_fumble;
      // Target number is nicely supplied with BetterRolls.
      trait_roll.rolls.forEach(roll => { dice.push( { value : roll.result, tn : roll.tn }) });
    }

    // Evaluate the dice rolls to get results.
    let result = evaluateDice(dice, isFumble, isWildcard);
    debug('result', result);

    // Create a new sequence.
    let sequence = new window.Sequence();
    // Set up target array.
    let targets = Array.from(game.user.targets);
    let numTargets = targets.length;
    // Iterate through the dice results.
    for (let i = 0; i < Object.keys(result).length; i++) {
      let target = targets[ i % numTargets ];
      // Fire the weapon at the target.
      fireWeapon(sequence, shooter, target, swadeItem.name, result[i]);
      if (isFumble) {
        // Only need one iteration in the fumble case.
        break;
      }
    }

    let delay = 0;
    let afterShotDelay = 0;
    let perDieDelay = 0;
    if (game.modules.get('dice-so-nice')?.active) {
      let dice;
      // Add in a delay for Dice So Nice! to allow time for the message to populate the chat window.
      // Calculate the number of exploding dice, as each one may need more delay.
      if (module == MODULE.SWADECORE || module == MODULE.SWADETOOLS) {
        afterShotDelay = DELAY.afterShotsSwade;
        perDieDelay = DELAY.perDieExplosionSwade;
        dice = rolls.map(roll => { return roll.result }).flat();
      } else {
        afterShotDelay = DELAY.afterShotsBetterRolls;
        perDieDelay = DELAY.perDieExplosionBetterRolls;
        dice = trait_roll.dice.map(roll => { return roll.results }).flat();
        rolls = trait_roll.dice;
      }
      // Add in extra delay for exploding dice.
      let explodes = dice.length - rolls.length;
      delay = afterShotDelay + (explodes * perDieDelay);
      debug('Adding a delay of ' + delay + ' to account for ' + explodes + ' extra dice being rolled:  ' +
            'base ' + afterShotDelay + ' + (' + explodes + ' * ' + perDieDelay + ' per die).');
    }

    // Play the sequence!
    timeoutId = setTimeout(() => {
      sequence.play();
    }, delay);
  }
}

// There can be a "global" or "outer" modifier that is not directly applied to each individual rolls.
// I.e. if the die formula is '{1d4x,1d6x}+5', the +5 is not applied to the individual roll totals, it
// has to be applied afterwards.  This function evaluates the roll terms and returns the modifier.
function getOuterModifier(terms) {
  let modifier = 0;
  if (terms.length == 3) {
    let term;
    // Operator term
    term = terms[1];
    let sign = (term instanceof OperatorTerm && term.operator == '-') ? -1 : 1;
    // Numeric term
    term = terms[2];
    let value = (term instanceof NumericTerm) ? term.number : 0;
    modifier = value * sign;
  }
  return modifier;
}

function evaluateDice(dice, isFumble, isWildcard) {
  debug('evaluateDice: isFumble ' + isFumble + ' isWildcard ' + isWildcard);
  let result = {};
  let lowestDie;
  let lowestValue = 0xFFFF;
  for (let i = 0; i < dice.length; i++) {
    let die = dice[i];
    result[i] = isFumble ? RESULT.FUMBLE : getResult(die);
    // Keep track of die with lowest value.
    if (die.value < lowestValue) {
      lowestValue = die.value;
      lowestDie = i;
    }
  }

  // the token is Wild card?
  if (isWildcard && !isFumble) {
    result[lowestDie] = RESULT.IGNORE;
    debug('Ignoring lowest die: ' + lowestDie);
  }

  return result;
}

// These result related functions assume data for a single die is passed in.
// Die format is final total and target number:  { value : #, tn : # }
function getResult(die) {
  let result = RESULT.FAIL;
  if (isRaise(die)) {
    result = RESULT.RAISE;
  } else if (isSuccess(die)) {
    result = RESULT.SUCCESS;
  }
  debug('getResult: value ' + die.value + ' tn ' + die.tn + ' result ' + result);
  return result;
}

function isRaise(die) {
  return (die.value >= (die.tn + 4));
}

function isSuccess(die) {
  return (die.value >= die.tn && die.value < (die.tn + 4));
}

function isFail(die) {
  return (die.value < die.tn);
}

// is fumble? for core and swade tools
export function isFumbleRoll(rolls) {
  debug('isFumbleRoll');
  if (rolls) {
    var numberOfRolls = rolls.length;
    var fumbleNumber = Math.floor((numberOfRolls / 2) + 1);
    var countOnes = 0;
    rolls.forEach(roll => {
      if (roll.result === 1) countOnes++;
    });
    debug('     numRolls ' + numberOfRolls + ' fumbleNumber ' + fumbleNumber + ' countOnes ' + countOnes);
    return ((countOnes >= fumbleNumber) && numberOfRolls > 1);
  } else {
    return false;
  }
}

// there is one fail in the dice? i think this is not the thing to check because i´m not taking in acount
//the modifiers but is only for the ricochet sound so is not that important, some of the 5, 10 or more bullets
//in Rof 2, 3, etc. can ricochet and its ok for ambience. (for core and swade tools only, in betterRoll they check the fail)
export function isFailWithRoFGreatherThanOne(rolls) {
  for (var i = 0; i < rolls.length; i++) {
    if (rolls[i].total < 4) return true;
  }
  return false;
}

