import {
    registerSettings,
} from './configuration/settings.js';
import {
    initBlood,
} from './blood.js';
import {
    initWeapons,
} from './weapons.js';
import {
    checkRollsAndShoot,
} from './utils.js';
import {
    moduleName,
    MODULE,
    debug,
} from './const.js';

export let DEADLANDS_WEAPON = {};
export let DEADLANDS_BLOOD = {};

// register the settings in the init
Hooks.once('init', () => {
    Handlebars.registerHelper('clean', function (aString) {
        return aString.replaceAll("'","");
    });
    registerSettings();
});

// Enable chat message hook after ready
Hooks.once('ready', () => {
    debug(moduleName + ' ready!');
    initWeapons();
    initBlood();
});

//Core Swade and swade-tools module case
Hooks.on('swadeAction', (swadeActor, swadeItem, actionType, roll, id) => {
    debug('swadeAction');
    //check the roll and shoot
    if (actionType != 'damage')
        checkRollsAndShoot(null, swadeItem, roll, MODULE.SWADECORE, swadeActor);
});

//betterroll case with hooks
Hooks.on('BRSW-RollItem', (message, html) => {
    debug('BRSW-RollItem hook');
    // get actor
    let actor = game.actors.get(message.actor_id);
    // get item
    let swadeItem = actor.items.find((item) => item.id === message.item_id);
    // check the roll and shoot
    checkRollsAndShoot(message.message, swadeItem, message.message.rolls, MODULE.BETTERROLLS, actor);
});