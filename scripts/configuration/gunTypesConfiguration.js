import {
  moduleName,
  SETTING_PREFIX,
  WEAPON,
  debug,
} from '../const.js';
import {
  initWeapons,
} from '../weapons.js';
import {
  DEADLANDS_WEAPON,
} from '../init.js';

export class gunTypesConfiguration extends FormApplication {

  // get the default options
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      id: 'gunTypesTitle-form',
      title: game.i18n.localize(SETTING_PREFIX + 'gunTypesPage.title'),
      template: './modules/' + moduleName + '/scripts/configuration/templates/gunTypesTemplate.html',
      closeOnSubmit: false
    });
  }

  // get the data for the page
  getData() {
    // Init guns dictionary to update it in cases new weapons were added.
    initWeapons();

    // Get boilerplate text phrases.
    let hint = game.i18n.localize(SETTING_PREFIX + 'weaponHint');
    let separated = game.i18n.localize(SETTING_PREFIX + 'separated');

    let settingList = [];
    Object.values(DEADLANDS_WEAPON).forEach(gun => {
      // Get weapon related phrases.
      let setting = gun.settingName;
      let title = game.i18n.localize(SETTING_PREFIX + setting);
      let weapon = title.toLowerCase();
      debug('Gun setting ' + setting);
      // Concatenate phrases to the proper template variables.
      settingList.push({
        gunTypesTitle : title,
        gunTypesName  : setting,
        gunTypesHint  : hint + weapon + separated,
        gunTypes      : game.settings.get(moduleName, setting)
      });
    });

    return { gunTypesList : settingList };
  }

  //create the listeners
  activateListeners(html) {
    //call the super listeners
    super.activateListeners(html);

    document.getElementById("gunTypes").addEventListener("drop", async (event) => {
      let data;
      try {
        data = JSON.parse(event.dataTransfer.getData("text/plain"));
        console.log(data);
        let itemName;
        if (data.type === "Item" || data.type === "item") {
          if (data.pack) {
            //from compendium
            let compendium = await game.packs.get(data.pack).getIndex();
            itemName = compendium.get(data.id).name
          } else {
              //from items
              itemName = game.items.get(data.id).name;
            }
          } else {
            if (data.type === "Macro" || data.type === "macro") {
              //from actors
              itemName = data.data.name.split(":")[1].trim();
            }
          }
          if (itemName) {
            document.getElementById("gunTypes").value = document.getElementById("gunTypes").value +
            (document.getElementById("gunTypes").value.trim() == "" ? "" : ",") + itemName;
          }
      } catch (err) {
        console.error(err);
      }
    });


    let ventana = this;
    ventana.render(true);
  }

  // update the object
  async _updateObject(event, formData) {
    //get the saved config data
    await game.settings.set(moduleName, formData.gunTypesName, formData.gunTypes.replaceAll("'",""));
    let ventana = this;
    ventana.render(true);
    //ui.notifications.info(game.i18n.localize("SWADESFXVFX.importingAnimationTemplate"));
  }
}
