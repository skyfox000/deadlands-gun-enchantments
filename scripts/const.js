//general cont
export const moduleName = "deadlands-gun-enchantments";
const MODS = 'modules/';

export const PATH = {
    AUDIO : MODS + moduleName + '/sounds/',
    VIDEO : MODS + moduleName + '/videos/',
}

export const SETTING_PREFIX = 'DeadlandsEnchantments.Settings.';
export const MATCH_ANY = '.*';

// Dice roll results.
export const RESULT = {
    IGNORE  : 'IGNORE',
    SUCCESS : 'SUCCESS',
    RAISE   : 'RAISE',
    FAIL    : 'FAIL',
    FUMBLE  : 'FUMBLE',
}

// Note:  no delay noticed for BetterRolls.
export const DELAY = {
    afterShotsSwade            : 1900,
    afterShotsBetterRolls      :    0,
    perDieExplosionSwade       :  700,
    perDieExplosionBetterRolls :    0,
}

export const MODULE = {
    SWADECORE   : 'SWADE-core',
    BETTERROLLS : 'BetterRolls',
    SWADETOOLS  : 'SWADE-tools',
}

// Flags that dictate behavior for shooting effects.
export const FLAG = {
    ROTATES   : 0x001,
    RICOCHET  : 0x002,
    CLICK     : 0x004,
    TWANG     : 0x008,
    FIRE      : 0x010,
    EXPLODES  : 0x020,
    BULLET    : 0x040,
    THUDS     : 0x080,
    ENERGY    : 0x100,
    DIRECTION : 0x200,
    MELEE     : 0x400,
}

// When adding new weapon types, add them here, in weapons.js, and in each lang/xx.js files.
export const WEAPON = {
    // Projectile weapons
    DERRINGER       : { 'setting' : 'derringer',      'default' : 'Derringer (.41) | Derringer' },
    SINGLE_ACTION   : { 'setting' : 'singleAction',   'default' : 'Colt Peacemaker (.45) | Colt Peacemaker' },
    DOUBLE_ACTION   : { 'setting' : 'doubleAction',   'default' : 'Colt Thunderer (.41) | Colt Thunderer' },
    CARBINE         : { 'setting' : 'carbine',        'default' : 'Spencer (.56) | Spencer' },
    RIFLE           : { 'setting' : 'rifle',          'default' : 'Winchester ‘73 (.44–40) | Winchester ‘73' },
    SHOTGUN         : { 'setting' : 'shotgun',        'default' : 'Colt Revolving Shotgun | Double-Barrel Shotgun | Shotgun' },
    GATLING         : { 'setting' : 'gatling',        'default' : 'Gatling Revolver (.36) | Gatling Revolver' },
    AUTOMATIC       : { 'setting' : 'automatic',      'default' : 'Glock (9 mm) | Glock' },
    ASSAULT         : { 'setting' : 'assault',        'default' : 'M-16 (5.56 mm) | M-16' },
    LASER           : { 'setting' : 'laser',          'default' : 'Laser Pistol | Laser Rifle' },
    LASER_MACHINE   : { 'setting' : 'laserMachine',   'default' : 'Laser Machine Gun' },
    FLAME           : { 'setting' : 'flame',          'default' : 'Flamethrower' },
    BOW             : { 'setting' : 'bow',            'default' : 'Bow' },
    ROCKET          : { 'setting' : 'rocket',         'default' : 'Rocket | Missile | Bazooka | Chinese Dragon' },
    GRENADE         : { 'setting' : 'grenade',        'default' : 'Grenade' },
    DYNAMITE        : { 'setting' : 'dynamite',       'default' : 'Dynamite | Dynamite/Nitro' },
    // Thrown weapons
    HATCHET         : { 'setting' : 'hatchet',        'default' : 'Axe | Axe, Throwing | Hatchet' },
    KNIFE           : { 'setting' : 'knife',          'default' : 'Knife | Bowie Knife' },
    TOMAHAWK        : { 'setting' : 'tomahawk',       'default' : 'Tomahawk' },
    SPEAR           : { 'setting' : 'spear',          'default' : 'Spear' },
    HAMMER          : { 'setting' : 'hammer',         'default' : 'Hammer | Hammer, Throwing' },
    STONE           : { 'setting' : 'stone',          'default' : 'Rock | Stone' },
    //Static Weapons
    PUNCH           : { 'setting' : 'punch',          'default' : 'Punch' }
}

// Source for artistic content.
const SRC = {
    NEFARRIO : 'Nefarrio#1770 @Discord',
    JACK     : 'Jack Kerouac\'s Animated Spell Effects',  // https://github.com/jackkerouac/animated-spell-effects
    JB2A     : 'JB2A-Jules&Ben\'s Animated Assets!',      // https://jb2a.com/
    JINKER   : 'Jinker\'s Animated Art Pack',             // https://github.com/jinkergm/JAA
    TALON    : 'United Cutlery M48 Talon Survival Spear', // https://www.unitedcutlery.com
    STONE    : 'Wikipedia Húsafell Stone',                // https://en.wikipedia.org/wiki/H%C3%BAsafell_Stone
    UNKNOWN  : ''                                         // Unknown source - if discovered, please raise an issue so it can be attributed.
}

// When adding new animations, add them here, in weapons.js, and in each lang/xx.js files.
export const ANIMATION = {
    // Projectile weapons
    FLASH         : { 'setting' : 'Flash',          'source' : SRC.JACK,     'default' : PATH.VIDEO + 'shotgun_blast_01.webm' },
    BARRAGE_FLASH : { 'setting' : 'BarrageFlash',   'source' : SRC.JINKER,   'default' : PATH.VIDEO + 'gunMuzzleFlash.webm' },
    SINGLE_SHOT   : { 'setting' : 'SingleShot',     'source' : SRC.JB2A,     'default' : PATH.VIDEO + 'Bullet_01_Regular_Orange_30ft_1600x400.webm' },
    SHOTGUN       : { 'setting' : 'Shotgun',        'source' : SRC.UNKNOWN,  'default' : PATH.VIDEO + 'shotgun_blast_01.webm' },
    BARRAGE       : { 'setting' : 'Barrage',        'source' : SRC.JACK,     'default' : PATH.VIDEO + 'bullet_barrage_01.webm' },
    LASER         : { 'setting' : 'Laser',          'source' : SRC.JB2A,     'default' : PATH.VIDEO + 'LaserShot_01_Regular_Blue_30ft_1600x400.webm' },
    LASER_BARRAGE : { 'setting' : 'BarrageLaser',   'source' : SRC.JACK,     'default' : PATH.VIDEO + 'laser_barrage_01.webm' },
    FLAME         : { 'setting' : 'Flamethrower',   'source' : SRC.JACK,     'default' : PATH.VIDEO + 'fire_ball_RAY_03.webm' },
    BOW           : { 'setting' : 'Bow',            'source' : SRC.JACK,     'default' : PATH.VIDEO + 'Arrow01_01_Regular_White_30ft_1600x400.webm' },
    ROCKET        : { 'setting' : 'Rocket',         'source' : SRC.UNKNOWN,  'default' : PATH.VIDEO + 'rocket2.webm' },
    GRENADE       : { 'setting' : 'Grenade',        'source' : SRC.UNKNOWN,  'default' : PATH.VIDEO + 'grenade.png' },
    DYNAMITE      : { 'setting' : 'Dynamite',       'source' : SRC.NEFARRIO, 'default' : PATH.VIDEO + 'dynamite.webp' },
    // Thrown weapons
    HATCHET       : { 'setting' : 'Hatchet',        'source' : SRC.NEFARRIO, 'default' : PATH.VIDEO + 'hatchet.webp' },
    KNIFE         : { 'setting' : 'Knife',          'source' : SRC.JB2A,     'default' : PATH.VIDEO + 'Dagger01_01_Regular_White_30ft_1600x400.webm' },
    TOMAHAWK      : { 'setting' : 'Tomahawk',       'source' : SRC.UNKNOWN,  'default' : PATH.VIDEO + 'tomahawk.webp' },
    SPEAR         : { 'setting' : 'Spear',          'source' : SRC.TALON,    'default' : PATH.VIDEO + 'spear.webp' },
    HAMMER        : { 'setting' : 'Hammer',         'source' : SRC.NEFARRIO, 'default' : PATH.VIDEO + 'hammer.webp' },
    STONE         : { 'setting' : 'Stone',          'source' : SRC.STONE,    'default' : PATH.VIDEO + 'stone.webp' },
    //Static Weapons
    PUNCH         : { 'setting' : 'Punch',          'source' : SRC.JB2A,     'default' : PATH.VIDEO + 'Library_Generic_Unarmed_Attacks_Unarmed_Strike_UnarmedStrike_01_Regular_Blue_Physical02_800x600.webm' }
}

// When adding new blood types, add them here and in each lang/xx.js files.
export const BLOOD = {
    BLACK  : { 'setting' : 'blackBlood',  'default' : 'Robot | Zombie' },
    BLUE   : { 'setting' : 'blueBlood',   'default' : 'Octopus' },
    GREEN  : { 'setting' : 'greenBlood',  'default' : 'Alien' },
    //PURPLE : { 'setting' : 'purpleBlood', 'creatures' : 'Worm' },
    RED    : { 'setting' : 'redBlood',    'default' : MATCH_ANY },
}

let DEBUG = false;
export function debug(msg, ...args) {
  if (DEBUG) {
    console.debug(msg, ...args);
  }
}
